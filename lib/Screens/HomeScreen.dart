import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gallai/Screens/expanded_album.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:camera_camera/camera_camera.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  FlutterSecureStorage storage = FlutterSecureStorage();
  bool circular = false;
  @override
  void initState() {
    super.initState();
    checkImages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
            child: circular
                ? Center(
                    child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                  ))
                : SafeArea(
                    child: Scaffold(
                      body: Container(
                          child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(30, 40, 0, 0),
                            child: Row(
                              children: [
                                Text("Albums",
                                    style: GoogleFonts.assistant(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 40,
                                    )),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(70, 5, 0, 0),
                                  child: GestureDetector(
                                    onTap: () {
                                      logOut();
                                      Navigator.pop(context);
                                    },
                                    child: Icon(
                                      Icons.logout,
                                      color: Colors.grey[400],
                                      size: 40,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(50, 5, 0, 0),
                                  child: GestureDetector(
                                    onTap: (){
                                      CameraCamera(
                                          onFile: (file) {
                                            Navigator.pop(context);
                                          }
                                      );
                                    },
                                    child: Icon(
                                      Icons.camera_alt,
                                      color: Colors.grey[400],
                                      size: 40,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(40, 30, 0, 0),
                            child: Row(
                              children: [
                                Text(
                                  "Memes",
                                  style: GoogleFonts.assistant(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 20,
                                  ),
                                ),
                                Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(180, 2, 0, 0),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  Expanded_Albumn(
                                                    "Memes",
                                                  )),
                                        );
                                      },
                                      child: Text(
                                        "See All",
                                        style: GoogleFonts.assistant(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15,
                                            color: Colors.grey),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(40, 10, 50, 0),
                            child: Container(
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Image.asset('assets/images/meme.jpg',
                                      width: 450.0,
                                      height: 120.0,
                                      fit: BoxFit.cover)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(40, 30, 0, 0),
                            child: Row(
                              children: [
                                Text(
                                  "Screenshots",
                                  style: GoogleFonts.assistant(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 20,
                                  ),
                                ),
                                Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(130, 2, 0, 0),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  Expanded_Albumn(
                                                    "Screenshots",
                                                  )),
                                        );
                                      },
                                      child: Text(
                                        "See All",
                                        style: GoogleFonts.assistant(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15,
                                            color: Colors.grey),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(40, 10, 50, 0),
                            child: Container(
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Image.asset(
                                      'assets/images/screenshot.jpg',
                                      width: 450.0,
                                      height: 120.0,
                                      fit: BoxFit.cover)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(40, 30, 0, 0),
                            child: Row(
                              children: [
                                Text(
                                  "Person",
                                  style: GoogleFonts.assistant(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 20,
                                  ),
                                ),
                                Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(180, 2, 0, 0),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  Expanded_Albumn(
                                                    "Person",
                                                  )),
                                        );
                                      },
                                      child: Text(
                                        "See All",
                                        style: GoogleFonts.assistant(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15,
                                            color: Colors.grey),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(40, 10, 50, 0),
                            child: Container(
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Image.asset('assets/images/person.jpg',
                                      width: 450.0,
                                      height: 120.0,
                                      fit: BoxFit.cover)),
                            ),
                          )
                        ],
                      )),
                    ),
                  )));
  }

  logOut() async {
    setState(() {
      circular = true;
    });
    Map<String, String> data = {};
    var value = await storage.read(key: "token");
    var response = await http.post(
      "http://10.0.2.2:8000/rest-auth/logout/",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Token  " + value.toString()
      },
      body: json.encode(data),
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
//      Map<String, dynamic> output = json.decode(response.body);
//      if (output == "Success") {
      await storage.delete(key: "token");
//      }
      setState(() {
        circular = false;
      });
    }
  }

  checkImages() async {
    setState(() {
      circular = true;
    });
    Map<String, String> data = {};
    var value = await storage.read(key: "token");
    var response = await http.get(
      "http://10.0.2.2:8000/api/checkimage/",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Token  " + value.toString()
      },
      //     body: json.encode(data),
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
//      Map<String, dynamic> output = json.decode(response.body);
//      if (output == "Success") {

//      }
      setState(() {
        circular = false;
      });
    } else {
      var imageList = Directory('/storage/emulated/0/DCIM/Camera')
          .listSync()
          .map((item) => item.path)
          .where((item) => item.endsWith(".jpg"))
          .toList();

      var value = await storage.read(key: "token");
      imageList.forEach((element) async {
        Map<String, String> data = {
          "img": element,
          "is_category": "Person",
          "owner": null
        };
        var response = await http.post(
          "http://10.0.2.2:8000/api/uploadimages/",
          headers: {
            "Content-type": "application/json",
            "Authorization": "Token  " + value.toString()
          },
          body: json.encode(data),
        );
      });
      setState(() {
        circular = false;
      });
      SnackBar(content: Text(response.body));
    }
  }
}
