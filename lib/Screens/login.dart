import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:gallai/Screens/HomeScreen.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool circular = false;
  FlutterSecureStorage storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: circular
              ? Center(
                  child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                ))
              : Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Container(
                        width: 600.0,
                        height: 400.0,
                        child: Image(
                            image: AssetImage('assets/images/process.png')),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0, 10),
                                        blurRadius: 20.0)
                                  ]),
                              child: Column(
                                children: [
                                  Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.grey[100]))),
                                      child: TextFormField(
                                        controller: _usernameController,
                                        decoration: InputDecoration(
                                            hintText: 'username',
                                            border: InputBorder.none,
                                            hintStyle: TextStyle(
                                                color: Colors.grey[400])),
                                      )),
                                  Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.grey[100]))),
                                      child: TextFormField(
                                        controller: _emailController,
                                        validator: (input) =>
                                            input.isValidEmail()
                                                ? null
                                                : "Check your email",
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        decoration: InputDecoration(
                                            hintText:
                                                'Email e.g. you@example.com',
                                            border: InputBorder.none,
                                            hintStyle: TextStyle(
                                                color: Colors.grey[400])),
                                      )),
                                  Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.grey[100]))),
                                      child: TextFormField(
                                        controller: _passwordController,
                                        obscureText: true,
                                        decoration: InputDecoration(
                                            hintText: "Password",
                                            border: InputBorder.none,
                                            hintStyle: TextStyle(
                                                color: Colors.grey[400])),
                                      )),
                                ],
                              ),
                            ),
                          ],
                        )),
                    SizedBox(
                      height: 50,
                      child: GestureDetector(
                        onTap: () {
                          loginIn();
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Container(
                            height: 40,
                            width: 300,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.redAccent),
                            child: Center(
                              child: Text(
                                "Login",
                                style: TextStyle(
                                    fontSize: 20,
                                    letterSpacing: 2,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          "Forget Password ?",
                          style: TextStyle(
                              fontSize: 15,
                              letterSpacing: 1,
                              color: Colors.grey[500]),
                        ),
                      ),
                    )
                  ],
                )),
    );
  }

  loginIn() async {
    setState(() {
      circular = true;
    });
    Map<String, String> data = {
      "username": _usernameController.text,
      "email": _emailController.text,
      "password": _passwordController.text,
    };

    var response = await http.post(
      "http://10.0.2.2:8000/rest-auth/login/",
      headers: {
        "Content-type": "application/json",
      },
      body: json.encode(data),
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      Map<String, dynamic> output = json.decode(response.body);
      await storage.write(key: "token", value: output["key"]);
      print(output["key"]);
      setState(() {
        circular = false;
      });
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => HomeScreen(),
          ),
          (route) => false);
    } else {
      setState(() {
        circular = false;
      });
      SnackBar(content: Text(response.body));
    }
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}
