import 'dart:collection';
import 'dart:io';
import 'image_view.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:drag_select_grid_view/drag_select_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Expanded_Albumn extends StatefulWidget {
  final String category;
  const Expanded_Albumn(this.category);
  @override
  _Expanded_AlbumnState createState() => _Expanded_AlbumnState();
}

class _Expanded_AlbumnState extends State<Expanded_Albumn> {
  Map<dynamic, dynamic> allImageInfo = new HashMap();
  final controller = DragSelectGridViewController();
  String category = "";
  List allImage = new List();
  List allNameList = new List();
  bool _loading = false;
  List _selected = new List();
  FlutterSecureStorage storage = FlutterSecureStorage();
  //var rec;
  //bool ch = false;
  @override
  void initState() {
    super.initState();
    getImages();
  }

  @override
  Widget build(BuildContext context) {
    return _loading
        ? Container(
            alignment: Alignment.center,
            color: Colors.white,
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          )
        : Scaffold(
            appBar: AppBar(
                elevation: 0,
                leading: IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.grey,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                backgroundColor: Colors.grey[100],
                title: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                      child: Text(
                        _selected.length < 1
                            ? " "
                            : "${_selected.length} item selected",
                        style: GoogleFonts.assistant(
                            fontWeight: FontWeight.w900,
                            fontSize: 20,
                            color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                      child: _selected.length < 1
                          ? Text(" ")
                          : IconButton(
                              onPressed: () {
                                deleteImages(_selected);
                                _selected.clear();
                                Navigator.pop(context);
                              },
                              icon: Icon(
                                Icons.delete_outline,
                                color: Colors.black,
                              ),
                            ),
                    )
                  ],
                )),
            body: Container(
              alignment: Alignment.center,
              child: _buildGrid(),
            ),
          );
  }

  Widget _buildGrid() {
    return GridView.extent(
        maxCrossAxisExtent: 150.0,
        padding: const EdgeInsets.all(4.0),
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        children: _buildGridTileList(allImage.length));
  }

  List<Container> _buildGridTileList(int count) {
    return List<Container>.generate(
        count,
        (int index) => Container(
                child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ImageView(
                              imagePath: allImage[index].toString(),
                              title: allImage[index].toString(),
                            )));
              },
              onLongPress: () {
                if (_selected.contains(allImage[index].toString())) {
                  setState(() {
                    _selected.remove(allImage[index].toString());
                  });
                } else {
                  setState(() {
                    _selected.add(allImage[index].toString());
                  });
                }
              },
              child: new Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _selected.contains(allImage[index].toString())
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Image.asset(
                            'assets/background/set.png',
                            width: 96.0,
                            height: 96.0,
                            fit: BoxFit.cover,
                          ),
                        )
                      : ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Image.network(
                            "http://10.0.2.2:8000" + allImage[index].toString(),
                            width: 96.0,
                            height: 96.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                ],
              ),
            )));
  }

  getImages() async {
    setState(() {
      _loading = true;
    });
    Map<String, String> data = {};
    var value = await storage.read(key: "token");
    var response = await http.get(
      "http://10.0.2.2:8000/api/imagesview/",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Token  " + value.toString()
      },
//      body: json.encode(data),
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      var output = json.decode(response.body);
      print(output);
      print(widget.category);
      int i;
      for (i = 0; i < output.length; i++) {
        print(output[i]["is_category"]);
        if (output[i]["is_category"] == widget.category) {
          allImage.add(output[i]["img"]);
        }
      }
      print(allImage);
      setState(() {
        _loading = false;
      });
    }
  }

  deleteImages(images) async {
    images.forEach((element) async {
      Map<String, String> data = {
        "img": "images/" + element,
        "is_category": "Person",
        "owner": null
      };

      var value = await storage.read(key: "token");
      var response = await http.post(
        "http://10.0.2.2:8000/api/deleteimage/",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Token  " + value.toString()
        },
        body: json.encode(data),
      );

      if (response.statusCode == 200 || response.statusCode == 201) {

      }
    });
  }
}
