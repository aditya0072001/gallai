import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF0700);
const kSecondaryColor = Color(0x585555);
const kTextColor = Color(0xFF757575);
